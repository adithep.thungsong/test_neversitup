import { IProductSchema } from './product.schema'
import { plainToClass } from 'class-transformer'
import { ObjectId } from 'mongodb'
import * as _ from 'lodash'
import { IRepositoryMapping } from '../../common/interface/repository.interface'
import { IProductModel } from '../../domain/product/interface/model.interface'
import { ProductModel } from '../../domain/product/product.model'

export class ProductRepositoryMapping implements IRepositoryMapping<IProductModel, IProductSchema> {
    public deserialize(schema: IProductSchema): IProductModel {
        return plainToClass(ProductModel, {
            _id: schema._id.toHexString(),
            _name: schema.name,
            _type: schema.type,
            _price: schema.price,
            _quantity: schema.quantity,
        })
    }

    public serialize(model: IProductModel): IProductSchema {
        return {
            _id: !_.isNil(model.getId()) ? new ObjectId(model.getId()) : new ObjectId(),
            name: model.getName(),
            type: model.getType(),
            price: model.getPrice(),
            quantity: model.getQuantity(),
        }
    }

}