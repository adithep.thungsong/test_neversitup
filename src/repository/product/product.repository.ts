import { Db } from 'mongodb'
import { IProductSchema } from './product.schema'
import { IRepositoryMapping, MongoCollectionNameEnum } from '../../common/interface/repository.interface'
import { MongoRepository } from '../../common/mongo-repository'
import { IProductModel } from '../../domain/product/interface/model.interface'
import { IProductRepository } from '../../domain/product/interface/repository.interface'

export class ProductRepository extends MongoRepository<IProductModel> implements IProductRepository {

    constructor(
        db: Db,
        mapping: IRepositoryMapping<IProductModel, IProductSchema>,
    ) {
        super(db.collection(MongoCollectionNameEnum.PRODUCT), mapping)
    }
}