import { ObjectId } from 'bson'

export interface IProductSchema {
    _id: ObjectId
    name: string
    type: string
    price: number
    quantity: number
}