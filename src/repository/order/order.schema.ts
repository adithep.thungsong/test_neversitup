import { ObjectId } from 'bson'

export interface IOrderSchema {
    _id: ObjectId
    userId: string
    productId: string
    amount: number
    status: OrderStatusEnum
    createdAt: Date
    updatedAt: Date
}

export enum OrderStatusEnum {
    SUCCESS = 'success',
    CANCEL = 'cancel',
}