import { plainToClass } from 'class-transformer'
import { ObjectId } from 'mongodb'
import * as _ from 'lodash'
import { IRepositoryMapping } from '../../common/interface/repository.interface'
import { IOrderSchema } from './order.schema'
import { IOrderModel } from '../../domain/order/interface/model.interface'
import { OrderModel } from '../../domain/order/order.model'

export class OrderRepositoryMapping implements IRepositoryMapping<IOrderModel, IOrderSchema> {
    public deserialize(schema: IOrderSchema): IOrderModel {
        return plainToClass(OrderModel, {
            _id: schema._id.toHexString(),
            _userId: schema.userId,
            _productId: schema.productId,
            _amount: schema.amount,
            _status: schema.status,
            _createdAt: schema.createdAt,
            _updatedAt: schema.updatedAt,
        })
    }

    public serialize(model: IOrderModel): IOrderSchema {
        return {
            _id: !_.isNil(model.getId()) ? new ObjectId(model.getId()) : new ObjectId(),
            userId: model.getUserId(),
            productId: model.getProductId(),
            amount: model.getAmount(),
            status: model.getStatus(),
            createdAt: model.getCreatedAt(),
            updatedAt: model.getUpdatedAt(),
        }
    }

}