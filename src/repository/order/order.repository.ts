import { Db, ObjectId } from 'mongodb'
import { IOrderSchema } from './order.schema'
import { defaultIfEmpty, from, map, Observable } from 'rxjs'
import { BadRequestException, InternalServerErrorException, NotFoundException } from '@nestjs/common'
import * as _ from 'lodash'
import { IRepositoryMapping, MongoCollectionNameEnum } from '../../common/interface/repository.interface'
import { MongoRepository } from '../../common/mongo-repository'
import { catchError } from 'rxjs/operators'
import { IOrderRepository } from '../../domain/order/interface/repository.interface'
import { IOrderModel } from '../../domain/order/interface/model.interface'

export class OrderRepository extends MongoRepository<IOrderModel> implements IOrderRepository {

    constructor(
        db: Db,
        mapping: IRepositoryMapping<IOrderModel, IOrderSchema>,
    ) {
        super(db.collection(MongoCollectionNameEnum.ORDER), mapping)
    }

    public getById(id: string): Observable<IOrderModel> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException(`Invalid Order ID`)
        }
        const promise = this.findOne({_id: new ObjectId(id)})
        return from(promise).pipe(
            defaultIfEmpty(null),
            map((orderModel: IOrderModel) => {
                if (_.isNil(orderModel)) {
                    throw new NotFoundException(`Order ID : ${id} Not Found`)
                }
                return orderModel
            }),
        )
    }

    public getByProductId(id: string): Observable<IOrderModel> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException(`Invalid Product ID`)
        }
        const promise = this.findOne({productId: id})
        return from(promise).pipe(
            defaultIfEmpty(null),
            map((orderModel: IOrderModel) => {
                if (_.isNil(orderModel)) {
                    throw new NotFoundException(`Product ID : ${id} Not Found`)
                }
                return orderModel
            }),
        )
    }

    public updateOrderStatus(id: string, field: object): Observable<boolean> {
        const promise = this._collection.findOneAndUpdate({_id: new ObjectId(id)}, {$set: field}, {
            returnDocument: 'after',
            upsert: false,
        })
        return from(promise).pipe(
            map((result) => {
                return _.isEqual(result.ok, 1)
            }),
            catchError((error) => {
                console.log(error)
                throw new InternalServerErrorException(('Cant upsert findAdnModify'))
            }),
        )
    }
}