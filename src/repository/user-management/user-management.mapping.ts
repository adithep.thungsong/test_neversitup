import { IUserManagementSchema } from './user-management.schema'
import { plainToClass } from 'class-transformer'
import { ObjectId } from 'mongodb'
import * as _ from 'lodash'
import { IUserManagementModel } from '../../domain/user-management/interface/model.interface'
import { UserManagementModel } from '../../domain/user-management/user-management.model'
import { IRepositoryMapping } from '../../common/interface/repository.interface'

export class UserManagementRepositoryMapping implements IRepositoryMapping<IUserManagementModel, IUserManagementSchema> {
    public deserialize(schema: IUserManagementSchema): IUserManagementModel {
        return plainToClass(UserManagementModel, {
            _id: schema._id.toHexString(),
            _username: schema.username,
            _password: schema.password,
            _name: schema.name,
            _gender: schema.gender,
            _address: schema.address,
            _email: schema.email,
            _telephone: schema.telephone,
            _createdAt: schema.createdAt,
        })
    }

    public serialize(model: IUserManagementModel): IUserManagementSchema {
        return {
            _id: !_.isNil(model.getId()) ? new ObjectId(model.getId()) : new ObjectId(),
            username: model.getUsername(),
            password: model.getPassword(),
            name: model.getName(),
            gender: model.getGender(),
            address: model.getAddress(),
            email: model.getEmail(),
            telephone: model.getTelephone(),
            createdAt: model.getCreatedAt(),
        }
    }

}