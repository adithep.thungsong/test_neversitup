import { Db, ObjectId } from 'mongodb'
import { IRepositoryMapping, MongoCollectionNameEnum } from '../../common/interface/repository.interface'
import { MongoRepository } from '../../common/mongo-repository'
import { IProductDetailRepository } from '../../domain/product-detail/interface/repository.interface'
import { IProductDetailSchema } from './product-detail.schema'
import { IProductDetailModel } from '../../domain/product-detail/interface/model.interface'
import { BadRequestException, NotFoundException } from '@nestjs/common'
import { defaultIfEmpty, from, map, Observable } from 'rxjs'
import * as _ from 'lodash'

export class ProductDetailRepository extends MongoRepository<IProductDetailModel> implements IProductDetailRepository {

    constructor(
        db: Db,
        mapping: IRepositoryMapping<IProductDetailModel, IProductDetailSchema>,
    ) {
        super(db.collection(MongoCollectionNameEnum.PRODUCT_DETAIL), mapping)
    }

    public getByProductId(id: string): Observable<IProductDetailModel> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException(`Invalid Product ID`)
        }
        const promise = this.findOne({productId: id})
        return from(promise).pipe(
            defaultIfEmpty(null),
            map((productDetailModel: IProductDetailModel) => {
                if (_.isNil(productDetailModel)) {
                    throw new NotFoundException(`Product ID : ${id} Not Found`)
                }
                return productDetailModel
            }),
        )
    }
}