import { ObjectId } from 'bson'

export interface IProductDetailSchema {
    _id: ObjectId
    productId: string
    detail: string
    detail2: string
    detail3: string
    detail4: string
}