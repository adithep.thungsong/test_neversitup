import { IProductDetailSchema } from './product-detail.schema'
import { plainToClass } from 'class-transformer'
import { ObjectId } from 'mongodb'
import * as _ from 'lodash'
import { IRepositoryMapping } from '../../common/interface/repository.interface'
import { IProductDetailModel } from '../../domain/product-detail/interface/model.interface'
import { ProductDetailModel } from '../../domain/product-detail/product.model'

export class ProductDetailRepositoryMapping implements IRepositoryMapping<IProductDetailModel, IProductDetailSchema> {
    public deserialize(schema: IProductDetailSchema): IProductDetailModel {
        return plainToClass(ProductDetailModel, {
            _id: schema._id.toHexString(),
            _productId: schema.productId,
            _detail: schema.detail,
            _detail2: schema.detail2,
            _detail3: schema.detail3,
            _detail4: schema.detail4,
        })
    }

    public serialize(model: IProductDetailModel): IProductDetailSchema {
        return {
            _id: !_.isNil(model.getId()) ? new ObjectId(model.getId()) : new ObjectId(),
            productId: model.getProductId(),
            detail: model.getDetail(),
            detail2: model.getDetail2(),
            detail3: model.getDetail3(),
            detail4: model.getDetail4(),
        }
    }

}