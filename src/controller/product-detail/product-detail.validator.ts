import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsOptional, IsString } from 'class-validator'
import { PaginatorValidator } from '../../common/validator'

export class ListProductValidator extends PaginatorValidator {
    @ApiProperty({
        name: 'type',
        required: false,
        type: String,
    })
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    private type: string

    public getType(): string {
        return this.type
    }
}