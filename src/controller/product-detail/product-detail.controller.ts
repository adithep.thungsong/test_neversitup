import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger'
import { Controller, Get, Inject, Param, UseGuards } from '@nestjs/common'
import { providerNames } from '../../provider/provider.name'
import { JwtAuthGuard } from '../../guards/jwt.guard'
import { IProductDetailService } from '../../domain/product-detail/interface/service.interface'

@ApiTags('Product Detail')
@Controller('/product-detail')
export class ProductDetailController {
    constructor(
        @Inject(providerNames.PRODUCT_DETAIL_SERVICE)
        private readonly _productDetailService: IProductDetailService,
    ) {}

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt-token')
    @ApiOperation({
        summary: 'get by product id',
    })
    @Get('/:id')
    public getByProductId(
        @Param('id') id: string,
    ) {
        return this._productDetailService.getByProductId(id)
    }
}