import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsOptional, IsString } from 'class-validator'
import { ILoginValidator, ILogoutValidator } from '../../domain/login/interface/validator.interface'

export class LoginValidator implements ILoginValidator {
    @ApiProperty({
        required: true,
        type: String,
    })
    @IsNotEmpty()
    @IsString()
    private username: string

    @ApiProperty({
        required: true,
        type: String,
    })
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    private password: string

    public getPassword(): string {
        return this.password
    }

    public getUsername(): string {
        return this.username
    }
}

export class LogoutValidator implements ILogoutValidator {
    @ApiProperty({
        required: true,
        type: String,
    })
    @IsNotEmpty()
    @IsString()
    private username: string

    public getUsername(): string {
        return this.username
    }
}