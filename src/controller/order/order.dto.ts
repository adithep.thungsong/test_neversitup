import { OrderStatusEnum } from '../../repository/order/order.schema'
import { IOrderModel } from '../../domain/order/interface/model.interface'
import * as _ from 'lodash'

export interface IOrderDto {
    id: string
    userId: string
    productId: string
    amount: number
    status: OrderStatusEnum
    createdAt: Date
    updatedAt: Date
}

export class OrderDto {
    public static toOrderDto(model: IOrderModel): IOrderDto {
        return {
            id: model.getId(),
            userId: model.getUserId(),
            productId: model.getProductId(),
            amount: model.getAmount(),
            status: model.getStatus(),
            createdAt: !_.isNil(model.getCreatedAt()) ? model.getCreatedAt() : null,
            updatedAt: !_.isNil(model.getUpdatedAt()) ? model.getUpdatedAt() : null,
        }
    }
}