import { ApiProperty } from '@nestjs/swagger'
import { IsEnum, IsIn, IsMongoId, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator'
import { PaginatorValidator } from '../../common/validator'
import { OrderStatusEnum } from '../../repository/order/order.schema'
import { IListOrderValidator, IOrderValidator } from '../../domain/order/interface/validator.interface'

export class ListOrderValidator extends PaginatorValidator implements IListOrderValidator{
    @ApiProperty({
        name: 'userId',
        required: false,
        type: String,
        example: '61e79628ccf567b0f2d552ce',
    })
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    @IsMongoId()
    private userId: string

    public getUserId(): string {
        return this.userId
    }

    @ApiProperty({
        name: 'productId',
        required: false,
        type: String,
        example: '61e79628ccf567b0f2d552ce',
    })
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    @IsMongoId()
    private productId: string

    public getProductId(): string {
        return this.productId
    }

    @ApiProperty({
        name: 'status',
        required: false,
        type: String,
        example: OrderStatusEnum.SUCCESS,
    })
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    @IsEnum(OrderStatusEnum)
    @IsIn([OrderStatusEnum.SUCCESS, OrderStatusEnum.CANCEL])
    private status: OrderStatusEnum

    public getStatus(): OrderStatusEnum {
        return this.status
    }
}

export class OrderValidator implements IOrderValidator {
    @ApiProperty({
        name: 'userId',
        required: true,
        type: String,
        example: '61e79628ccf567b0f2d552ce',
    })
    @IsNotEmpty()
    @IsString()
    @IsMongoId()
    private userId: string

    public getUserId(): string {
        return this.userId
    }

    @ApiProperty({
        name: 'productId',
        required: true,
        type: String,
        example: '61e79628ccf567b0f2d552ce',
    })
    @IsNotEmpty()
    @IsString()
    @IsMongoId()
    private productId: string

    public getProductId(): string {
        return this.productId
    }

    // @ApiProperty({
    //     name: 'status',
    //     required: false,
    //     type: String,
    //     example: OrderStatusEnum.SUCCESS,
    // })
    // @IsOptional()
    // @IsNotEmpty()
    // @IsString()
    // @IsEnum(OrderStatusEnum)
    // @IsIn([OrderStatusEnum.SUCCESS, OrderStatusEnum.CANCEL])
    // private status: OrderStatusEnum
    //
    // public getStatus(): OrderStatusEnum {
    //     return this.status
    // }

    @ApiProperty({
        name: 'amount',
        required: true,
        type: Number,
        example: 1,
    })
    @IsNotEmpty()
    @IsNumber()
    private amount: number

    public getAmount(): number {
        return this.amount
    }
}