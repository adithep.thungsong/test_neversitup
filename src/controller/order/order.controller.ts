import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger'
import { Body, Controller, Delete, Get, Inject, Param, Post, Query, UseGuards } from '@nestjs/common'
import { providerNames } from '../../provider/provider.name'
import { ListOrderValidator, OrderValidator } from './order.validator'
import { JwtAuthGuard } from '../../guards/jwt.guard'
import { mergeMap, reduce } from 'rxjs'
import { map } from 'rxjs/operators'
import * as _ from 'lodash'
import { IOrderDto, OrderDto } from './order.dto'
import { IOrderService } from '../../domain/order/interface/service.interface'
import { IOrderModel } from '../../domain/order/interface/model.interface'

@ApiTags('Order')
@Controller('/order')
export class OrderController {
    constructor(
        @Inject(providerNames.ORDER_SERVICE)
        private readonly _orderService: IOrderService,
    ) {}

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt-token')
    @ApiOperation({
        summary: 'get by id',
    })
    @Get('/:id')
    public getById(
        @Param('id') id: string,
    ) {
        return this._orderService.getById(id)
    }

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt-token')
    @ApiOperation({
        summary: 'order list',
    })
    @Get('/')
    public searchOrder(
        @Query() filter: ListOrderValidator,
    ) {
        const dtoTemplate = {
            totalAll: null,
            total: null,
            data: [],
        }
        return this._orderService.count(filter).pipe(
            mergeMap((countUser: number) => {
                return this._orderService.search(filter).pipe(
                    reduce((acc, orderModel: IOrderModel) => {
                        acc.push(OrderDto.toOrderDto(orderModel))
                        return acc
                    }, []),
                    map((data: IOrderDto[]) => {
                        dtoTemplate.totalAll = countUser
                        dtoTemplate.total = _.size(data)
                        dtoTemplate.data = data
                        return dtoTemplate
                    }),
                )
            }),
        )
    }

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt-token')
    @ApiOperation({
        summary: 'create order',
    })
    @Post('/')
    public create(
        @Body() body: OrderValidator,
    ) {
        return this._orderService.create(body)
    }

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt-token')
    @ApiOperation({
        summary: 'cancel order',
    })
    @Delete('/:orderId')
    public delete(
        @Param('orderId') id: string,
    ) {
        return this._orderService.cancelOrder(id)
    }
}