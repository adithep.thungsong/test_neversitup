import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger'
import { Controller, Get, Inject, Query, UseGuards } from '@nestjs/common'
import { providerNames } from '../../provider/provider.name'
import { ListProductValidator } from './product.validator'
import { JwtAuthGuard } from '../../guards/jwt.guard'
import { mergeMap, reduce } from 'rxjs'
import { map } from 'rxjs/operators'
import * as _ from 'lodash'
import { IProductDto, ProductDto } from './product.dto'
import { IProductModel } from '../../domain/product/interface/model.interface'
import { IProductService } from '../../domain/product/interface/service.interface'

@ApiTags('Product')
@Controller('/product')
export class ProductController {
    constructor(
        @Inject(providerNames.PRODUCT_SERVICE)
        private readonly _productService: IProductService,
    ) {}

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt-token')
    @ApiOperation({
        summary: 'product list',
    })
    @Get('/')
    public searchProduct(
        @Query() filter: ListProductValidator,
    ) {
        const dtoTemplate = {
            totalAll: null,
            total: null,
            data: [],
        }
        return this._productService.count(filter).pipe(
            mergeMap((countUser: number) => {
                return this._productService.search(filter).pipe(
                    reduce((acc, productModel: IProductModel) => {
                        acc.push(ProductDto.toProductDto(productModel))
                        return acc
                    }, []),
                    map((data: IProductDto[]) => {
                        dtoTemplate.totalAll = countUser
                        dtoTemplate.total = _.size(data)
                        dtoTemplate.data = data
                        return dtoTemplate
                    }),
                )
            }),
        )
    }
}