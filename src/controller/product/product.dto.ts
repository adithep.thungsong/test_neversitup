import { IProductModel } from '../../domain/product/interface/model.interface'

export interface IProductDto {
    id: string
    name: string
    type: string
    price: number
    quantity: number
}

export class ProductDto {
    public static toProductDto(model: IProductModel): IProductDto {
        return {
            id: model.getId(),
            name: model.getName(),
            type: model.getType(),
            price: model.getPrice(),
            quantity: model.getQuantity(),
        }
    }
}