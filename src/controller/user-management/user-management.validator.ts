import { ApiProperty } from '@nestjs/swagger'
import { IsEnum, IsIn, IsNotEmpty, IsOptional, IsString } from 'class-validator'
import { IRegisterValidator } from '../../domain/user-management/interface/validator.interface'
import { GenderEnum } from '../../repository/user-management/user-management.schema'

export class RegisterValidator implements IRegisterValidator {
    @ApiProperty({
        name: 'username',
        required: true,
        type: String,
    })
    @IsNotEmpty()
    @IsString()
    private username: string

    public getUsername(): string {
        return this.username
    }

    @ApiProperty({
        name: 'password',
        required: true,
        type: String,
    })
    @IsNotEmpty()
    @IsString()
    private password: string

    public getPassword(): string {
        return this.password
    }

    @ApiProperty({
        name: 'name',
        required: true,
        type: String,
        example: 'สมชาย ใจดี',
    })
    @IsNotEmpty()
    @IsString()
    private name: string

    public getName(): string {
        return this.name
    }

    @ApiProperty({
        name: 'gender',
        required: false,
        type: String,
        example: GenderEnum
    })
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    @IsEnum(GenderEnum)
    @IsIn([GenderEnum.MALE, GenderEnum.FEMALE, GenderEnum.OTHER])
    private gender: GenderEnum

    public getGender(): GenderEnum {
        return this.gender
    }

    @ApiProperty({
        name: 'address',
        required: true,
        type: String,
    })
    @IsNotEmpty()
    @IsString()
    private address: string

    public getAddress(): string {
        return this.address
    }

    @ApiProperty({
        name: 'email',
        required: false,
        type: String,
        example: '123@123.com'
    })
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    private email: string

    public getEmail(): string {
        return this.email
    }

    @ApiProperty({
        name: 'telephone',
        required: false,
        type: String,
    })
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    private telephone: string

    public getTelephone(): string {
        return this.telephone
    }
}