import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger'
import { Body, Controller, Get, Inject, Param, Post, UseGuards } from '@nestjs/common'
import { providerNames } from '../../provider/provider.name'
import { IUserManagementService } from '../../domain/user-management/interface/service.interface'
import { RegisterValidator } from './user-management.validator'
import { JwtAuthGuard } from '../../guards/jwt.guard'

@ApiTags('User-Management')
@Controller('/user-management')
export class UserManagementController {
    constructor(
        @Inject(providerNames.USER_MANAGEMENT_SERVICE)
        private readonly _userManagementService: IUserManagementService,
    ) {}

    @ApiOperation({
        summary: 'register',
    })
    @Post('/')
    public register(
        @Body() body: RegisterValidator,
    ) {
        return this._userManagementService.register(body)
    }

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt-token')
    @ApiOperation({
        summary: 'get by id',
    })
    @Get('/:id')
    public getById(
        @Param('id') id: string,
    ) {
        return this._userManagementService.getById(id)
    }
}