import { IProductModel } from './interface/model.interface'
import { Entity } from '../../common/entity'

export class ProductModel extends Entity implements IProductModel {
    private _name: string
    private _type: string
    private _price: number
    private _quantity: number

    public getName(): string {
        return this._name
    }

    public setName(name: string): void {
        this._name = name
    }

    public getType(): string {
        return this._type
    }

    public setType(type: string): void {
        this._type = type
    }

    public getPrice(): number {
        return this._price
    }

    public setPrice(price: number): void {
        this._price = price
    }

    public getQuantity(): number {
        return this._quantity
    }

    public setQuantity(quantity: number): void {
        this._quantity = quantity
    }
}

// export class ProductDetailModel extends Entity implements IProductDetailModel {
//     private _detail: string
//     private _detail2: string
//     private _detail3: string
//     private _detail4: string
//
//     public getDetail(): string {
//         return this._detail
//     }
//
//     public setDetail(detail: string): void {
//         this._detail = detail
//     }
//
//     public getDetail2(): string {
//         return this._detail2
//     }
//
//     public setDetail2(detail2: string): void {
//         this._detail2 = detail2
//     }
//
//     public getDetail3(): string {
//         return this._detail3
//     }
//
//     public setDetail3(detail3: string): void {
//         this._detail3 = detail3
//     }
//
//     public getDetail4(): string {
//         return this._detail4
//     }
//
//     public setDetail4(detail4: string): void {
//         this._detail4 = detail4
//     }
// }