import { IProductService } from './interface/service.interface'
import { Observable } from 'rxjs'
import { IProductRepository } from './interface/repository.interface'
import { IProductModel } from './interface/model.interface'
import * as _ from 'lodash'
import { IListProductValidator } from './interface/validator.interface'
import { IPaginationFilter } from '../../common/interface/schema.interface'

export class ProductService implements IProductService {
    constructor(
        private readonly _productRepository: IProductRepository,
    ) {
    }

    public search(input: IListProductValidator): Observable<IProductModel> {
        const filter = this._setFilter(input)
        const pagination: IPaginationFilter = {
            limit: !_.isNil(input.getLimit()) ? input.getLimit() : null,
            page: !_.isNil(input.getPage()) ? input.getPage() : null,
        }
        return this._productRepository.find(filter, pagination)
    }

    public count(input: IListProductValidator): Observable<number> {
        const filter = this._setFilter(input)
        return this._productRepository.count(filter)
    }

    private _setFilter(inputFilter: IListProductValidator) {
        const searchFilter = {}
        if (!_.isNil(inputFilter.getType())) {
            Object.assign(searchFilter, {
                'type': inputFilter.getType(),
            })
        }
        return searchFilter
    }
}