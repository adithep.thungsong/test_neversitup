import { IEntity } from '../../../common/interface/entity.interface'

export interface IProductModel extends IEntity {
    getName(): string
    setName(name: string): void

    getType(): string
    setType(type: string): void

    getPrice(): number
    setPrice(price: number): void

    getQuantity(): number
    setQuantity(quantity: number): void
}