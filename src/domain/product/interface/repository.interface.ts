import { IProductModel } from './model.interface'
import { IRepository } from '../../../common/interface/repository.interface'

export interface IProductRepository extends IRepository<IProductModel> {
    //
}