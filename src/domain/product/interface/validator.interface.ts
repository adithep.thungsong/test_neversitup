import { IPaginationValidator } from '../../../common/interface/validator.interface'

export interface IListProductValidator extends IPaginationValidator {
    getType(): string
}