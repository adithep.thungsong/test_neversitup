import { Observable } from 'rxjs'
import { IProductModel } from './model.interface'
import { IListProductValidator } from './validator.interface'

export interface IProductService {
    search(input: IListProductValidator): Observable<IProductModel>

    count(input: IListProductValidator): Observable<number>
}

