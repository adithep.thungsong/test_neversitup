import { IProductDetailModel } from './model.interface'
import { Observable } from 'rxjs'
import { IRepository } from '../../../common/interface/repository.interface'

export interface IProductDetailRepository extends IRepository<IProductDetailModel> {
    getByProductId(id: string): Observable<IProductDetailModel>
}