import { IEntity } from '../../../common/interface/entity.interface'

export interface IProductDetailModel extends IEntity {
    getProductId(): string
    setProductId(productId: string): void

    getDetail(): string
    setDetail(detail: string): void

    getDetail2(): string
    setDetail2(detail2: string): void

    getDetail3(): string
    setDetail3(detail3: string): void

    getDetail4(): string
    setDetail4(detail4: string): void
}