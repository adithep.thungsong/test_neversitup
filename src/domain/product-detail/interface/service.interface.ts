import { Observable } from 'rxjs'
import { IProductDetailModel } from './model.interface'

export interface IProductDetailService {
    getByProductId(id: string): Observable<IProductDetailModel>
}

