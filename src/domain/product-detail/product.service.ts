import { IProductDetailService } from './interface/service.interface'
import { Observable } from 'rxjs'
import { IProductDetailRepository } from './interface/repository.interface'
import { IProductDetailModel } from './interface/model.interface'

export class ProductDetailService implements IProductDetailService {
    constructor(
        private readonly _productDetailRepository: IProductDetailRepository,
    ) {
    }

    public getByProductId(id: string): Observable<IProductDetailModel> {
        return this._productDetailRepository.getByProductId(id)
    }
}