import { IProductDetailModel } from './interface/model.interface'
import { Entity } from '../../common/entity'

export class ProductDetailModel extends Entity implements IProductDetailModel {
    private _productId: string
    private _detail: string
    private _detail2: string
    private _detail3: string
    private _detail4: string

    public getProductId(): string {
        return this._productId
    }

    public setProductId(productId: string): void {
        this._productId = productId
    }

    public getDetail(): string {
        return this._detail
    }

    public setDetail(detail: string): void {
        this._detail = detail
    }

    public getDetail2(): string {
        return this._detail2
    }

    public setDetail2(detail2: string): void {
        this._detail2 = detail2
    }

    public getDetail3(): string {
        return this._detail3
    }

    public setDetail3(detail3: string): void {
        this._detail3 = detail3
    }

    public getDetail4(): string {
        return this._detail4
    }

    public setDetail4(detail4: string): void {
        this._detail4 = detail4
    }
}