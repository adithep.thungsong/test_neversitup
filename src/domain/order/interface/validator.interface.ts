import { OrderStatusEnum } from '../../../repository/order/order.schema'
import { IPaginationValidator } from '../../../common/interface/validator.interface'

export interface IOrderValidator {
    getUserId(): string

    getProductId(): string

    getAmount(): number

    // getStatus(): OrderStatusEnum
}

export interface IListOrderValidator extends IPaginationValidator {
    getUserId(): string

    getProductId(): string

    getStatus(): OrderStatusEnum
}