import { IEntity } from '../../../common/interface/entity.interface'
import { OrderStatusEnum } from '../../../repository/order/order.schema'

export interface IOrderModel extends IEntity {
    getUserId(): string
    setUserId(userId: string): void

    getProductId(): string
    setProductId(productId: string): void

    getAmount(): number
    setAmount(amount: number): void

    getStatus(): OrderStatusEnum
    setStatus(status: OrderStatusEnum): void

}