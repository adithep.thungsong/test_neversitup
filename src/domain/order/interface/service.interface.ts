import { Observable } from 'rxjs'
import { IOrderModel } from './model.interface'
import { IListOrderValidator, IOrderValidator } from './validator.interface'

export interface IOrderService {
    create(input: IOrderValidator): Observable<string>

    getById(id: string): Observable<IOrderModel>

    cancelOrder(id: string): Observable<boolean>

    search(input: IListOrderValidator): Observable<IOrderModel>

    count(input: IListOrderValidator): Observable<number>
}

