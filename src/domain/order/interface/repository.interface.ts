import { IOrderModel } from './model.interface'
import { Observable } from 'rxjs'
import { IRepository } from '../../../common/interface/repository.interface'

export interface IOrderRepository extends IRepository<IOrderModel> {
    getById(id: string): Observable<IOrderModel>

    getByProductId(id: string): Observable<IOrderModel>

    updateOrderStatus(id: string, field: object): Observable<boolean>
}