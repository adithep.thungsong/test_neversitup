import { IOrderService } from './interface/service.interface'
import { defaultIfEmpty, forkJoin, mergeMap, Observable } from 'rxjs'
import { IOrderRepository } from './interface/repository.interface'
import { OrderBuilder } from './order.builder'
import { IListOrderValidator, IOrderValidator } from './interface/validator.interface'
import { IOrderModel } from './interface/model.interface'
import { OrderStatusEnum } from '../../repository/order/order.schema'
import { IPaginationFilter } from '../../common/interface/schema.interface'
import * as _ from 'lodash'
import { IProductRepository } from '../product/interface/repository.interface'
import { IUserManagementRepository } from '../user-management/interface/repository.interface'
import { map } from 'rxjs/operators'
import { ObjectId } from 'mongodb'
import { IProductModel } from '../product/interface/model.interface'
import { IUserManagementModel } from '../user-management/interface/model.interface'
import { NotFoundException } from '@nestjs/common'

export class OrderService implements IOrderService {
    constructor(
        private readonly _orderRepository: IOrderRepository,
        private readonly _productRepository: IProductRepository,
        private readonly _userManagementRepository: IUserManagementRepository,
    ) {
    }

    public create(input: IOrderValidator): Observable<string> {
        return this._productRepository.findOne({_id: new ObjectId(input.getProductId())}).pipe(
            defaultIfEmpty(null),
            mergeMap((productModel: IProductModel) => {
                if(_.isNil(productModel)){
                    throw new NotFoundException(`Product ID: ${input.getProductId()} Not Found`)
                }
                return this._userManagementRepository.getById(input.getUserId()).pipe(
                    defaultIfEmpty(null),
                    mergeMap((userModel: IUserManagementModel) => {
                        if(_.isNil(userModel)){
                            throw new NotFoundException(`User ID: ${input.getUserId()} Not Found`)
                        }

                        const newOrder = new OrderBuilder()
                        newOrder.setUserId(input.getUserId())
                        newOrder.setProductId(input.getProductId())
                        newOrder.setAmount(input.getAmount())
                        newOrder.setStatus(OrderStatusEnum.SUCCESS)
                        productModel.setQuantity(productModel.getQuantity() - input.getAmount())
                        return forkJoin([
                            this._orderRepository.insert(newOrder.build()),
                            this._productRepository.updateByObjectId(productModel.getId(), productModel),
                        ]).pipe(
                            map((result: any[]) => {
                                return result[0]
                            })
                        )
                    })
                )
            })
        )
    }

    public getById(id: string): Observable<IOrderModel> {
        return this._orderRepository.getById(id)
    }

    public cancelOrder(id: string): Observable<boolean> {
        return this._orderRepository.findOne({_id: new ObjectId(id)}).pipe(
            defaultIfEmpty(null),
            mergeMap((orderModel: IOrderModel) => {
                if(_.isNil(orderModel)){
                    throw new NotFoundException(`Order ID: ${id} Not Found`)
                }
                const updateFilter: object = {
                    status: OrderStatusEnum.CANCEL,
                    updatedAt: new Date(),
                }
                return this._orderRepository.updateOrderStatus(id, updateFilter)
            })
        )
    }

    public search(input: IListOrderValidator): Observable<IOrderModel> {
        const filter = this._setFilter(input)
        const pagination: IPaginationFilter = {
            limit: !_.isNil(input.getLimit()) ? input.getLimit() : null,
            page: !_.isNil(input.getPage()) ? input.getPage() : null,
        }
        return this._orderRepository.find(filter, pagination)
    }

    public count(input: IListOrderValidator): Observable<number> {
        const filter = this._setFilter(input)
        return this._orderRepository.count(filter)
    }

    private _setFilter(inputFilter: IListOrderValidator) {
        const searchFilter = {}
        if (!_.isNil(inputFilter.getProductId())) {
            Object.assign(searchFilter, {
                'productId': inputFilter.getProductId(),
            })
        }
        if (!_.isNil(inputFilter.getUserId())) {
            Object.assign(searchFilter, {
                'userId': inputFilter.getUserId(),
            })
        }
        if (!_.isNil(inputFilter.getStatus())) {
            Object.assign(searchFilter, {
                'status': inputFilter.getStatus(),
            })
        }
        return searchFilter
    }
}