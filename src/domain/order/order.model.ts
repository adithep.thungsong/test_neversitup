import { IOrderModel } from './interface/model.interface'
import { Entity } from '../../common/entity'
import { OrderStatusEnum } from '../../repository/order/order.schema'

export class OrderModel extends Entity implements IOrderModel {
    private _userId: string
    private _productId: string
    private _amount: number
    private _status: OrderStatusEnum

    public getUserId(): string {
        return this._userId
    }

    public setUserId(userId: string): void {
        this._userId = userId
    }

    public getProductId(): string {
        return this._productId
    }

    public setProductId(productId: string): void {
        this._productId = productId
    }

    public getAmount(): number {
        return this._amount
    }

    public setAmount(amount: number): void {
        this._amount = amount
    }

    public getStatus(): OrderStatusEnum {
        return this._status
    }

    public setStatus(status: OrderStatusEnum): void {
        this._status = status
    }
}