import { IOrderModel } from './interface/model.interface'
import { OrderModel } from './order.model'
import { OrderStatusEnum } from '../../repository/order/order.schema'

export class OrderBuilder {
    private readonly _model: IOrderModel

    constructor() {
        this._model = new OrderModel()
    }

    public build(): IOrderModel {
        return this._model
    }

    public setUserId(userId: string) {
        this._model.setUserId(userId)
        return this
    }

    public setProductId(productId: string) {
        this._model.setProductId(productId)
        return this
    }

    public setAmount(amount: number) {
        this._model.setAmount(amount)
        return this
    }

    public setStatus(status: OrderStatusEnum) {
        this._model.setStatus(status)
        return this
    }
}