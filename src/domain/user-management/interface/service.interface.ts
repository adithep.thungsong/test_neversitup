import { Observable } from 'rxjs'
import { IRegisterValidator } from './validator.interface'
import { IUserManagementModel } from './model.interface'

export interface IUserManagementService {
    register(data: IRegisterValidator): Observable<string>

    getById(id: string): Observable<IUserManagementModel>
}

