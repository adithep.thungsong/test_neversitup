import { GenderEnum } from '../../../repository/user-management/user-management.schema'

export interface IRegisterValidator {
    getUsername(): string

    getPassword(): string

    getName(): string

    getGender(): GenderEnum

    getAddress(): string

    getEmail(): string

    getTelephone(): string
}