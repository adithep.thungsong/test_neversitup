import { IUserManagementModel } from './interface/model.interface'
import { Entity } from '../../common/entity'
import { GenderEnum } from '../../repository/user-management/user-management.schema'

export class UserManagementModel extends Entity implements IUserManagementModel {
    private _username: string
    private _password: string
    private _name: string
    private _gender: GenderEnum
    private _address: string
    private _email: string
    private _telephone: string

    constructor() {
        super()
        this._email = null
        this._telephone = null
    }

    public getUsername(): string {
        return this._username
    }

    public setUsername(username: string): void {
        this._username = username
    }

    public getPassword(): string {
        return this._password
    }

    public setPassword(password: string): void {
        this._password = password
    }

    public getName(): string {
        return this._name
    }

    public setName(name: string): void {
        this._name = name
    }

    public getGender(): GenderEnum {
        return this._gender
    }

    public setGender(gender: GenderEnum): void {
        this._gender = gender
    }

    public getAddress(): string {
        return this._address
    }

    public setAddress(address: string): void {
        this._address = address
    }

    public getEmail(): string {
        return this._email
    }

    public setEmail(email: string): void {
        this._email = email
    }

    public getTelephone(): string {
        return this._telephone
    }

    public setTelephone(telephone: string): void {
        this._telephone = telephone
    }
}