import { LoginValidator } from '../../../controller/login/login.validator'
import { Observable } from 'rxjs'
import { IncomingHttpHeaders } from 'http'

export interface ILoginService {
    login(payload: LoginValidator, httpHeaders: IncomingHttpHeaders): Observable<string>
}

export interface IUserPayloadJWT {
    id: string
    username: string
    sessionId: string
}