export interface ILoginValidator {
    getUsername(): string

    getPassword(): string
}

export interface ILogoutValidator {
    getUsername(): string
}