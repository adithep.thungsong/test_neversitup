import { IEntity } from './interface/entity.interface'
import {
    from,
    Observable,
    Observer,
    of,
} from 'rxjs'
import {
    AggregationCursor,
    Collection,
    Cursor,
    ObjectId,
} from 'mongodb'
import {
    IRepository,
    IRepositoryMapping,
    SortDirectionEnum,
} from './interface/repository.interface'
import { Logger } from '@nestjs/common'
import { IPaginationFilter } from './interface/schema.interface'
import * as _ from 'lodash'
import {
    catchError,
    map,
} from 'rxjs/operators'

export class MongoRepository<M extends IEntity> implements IRepository<M> {
    protected _collection: Collection
    protected _mapper: IRepositoryMapping<M, any>
    private _logger: Logger

    protected constructor(collection: Collection, mapper: IRepositoryMapping<M, any>) {
        this._collection = collection
        this._mapper = mapper
        this._logger = new Logger('MongoRepository')
    }

    public toDocument(model: M): any {
        return this._mapper.serialize(model)
    }

    public toModel(object: any): M {
        return this._mapper.deserialize(object)
    }

    public toRawObservable(source: Cursor): Observable<any> {
        const observablePipe = ((observer: Observer<M>) => {
            source
                .on('data', (document: any) => {
                    observer.next(document)
                })
                .on('end', () => {
                    source.close()
                    observer.complete()
                })
                .on('error', (err: Error) => {
                    source.close()
                    observer.error(err)
                })
        })
        return new Observable(observablePipe)
    }

    public toObservableFromAggregateCursor(source: AggregationCursor): Observable<M> {
        const self = this
        const observablePipe = ((observer: Observer<M>) => {
            source
                .on('data', (document: any) => {
                    observer.next(self.toModel(document))
                })
                .on('end', () => {
                    source.close()
                    observer.complete()
                })
                .on('error', (err: Error) => {
                    source.close()
                    observer.error(err)
                })
        })
        return new Observable(observablePipe)
    }

    public toRawObservableFromAggregateCursor(source: AggregationCursor): Observable<any> {
        const observablePipe = (observer: Observer<M>) => {
            source
                .on('data', (document: any) => {
                    observer.next(document)
                })
                .on('end', () => {
                    source.close()
                    observer.complete()
                })
                .on('error', (err: Error) => {
                    source.close()
                    observer.error(err)
                })
        }
        return new Observable(observablePipe)
    }

    public toObservable(source: Cursor): Observable<M> {
        const self = this
        const observablePipe = ((observer: Observer<M>) => {
            source.stream()
                .on('data', (document: any) => {
                    observer.next(self.toModel(document))
                })
                .on('end', () => {
                    source.close()
                    observer.complete()
                })
                .on('error', (err: Error) => {
                    source.close()
                    observer.error(err)
                })
        })
        return new Observable(observablePipe)
    }

    public list(page: number = 1, limit: number = 20): Observable<any> {
        const startFrom = (page - 1) * limit
        const mongoCursor = this._collection.find().skip(startFrom).limit(limit)
        return from(this.toObservable(mongoCursor))
    }

    public find(filter?: Object, pagination?: IPaginationFilter, options?: any, sortBy?: string, sortDirection?: SortDirectionEnum): Observable<M> {
        const cursor = this._collection.find(filter, {projection: options})
        const sortObj = {}
        if (!_.isNil(sortBy)) {
            if (!_.isNil(sortDirection)) {
                Object.assign(sortObj, {
                    [sortBy]: sortDirection,
                })
            } else {
                Object.assign(sortObj, {
                    [sortBy]: -1,
                })
            }
        } else {
            Object.assign(sortObj, {
                createdAt: -1,
            })
        }
        cursor.sort(sortObj)
        if (!_.isNil(pagination) && !_.isNil(pagination.limit)) {
            cursor.limit(pagination.limit)
            if (!_.isNil(pagination.page)) {
                cursor.skip(pagination.limit * (pagination.page - 1))
            }
        }
        return this.toObservable(cursor)
    }

    public findOne(filter: Object): Observable<M> {
        const promise = this._collection.findOne(filter)
        return from(promise).pipe(
            map((result) => {
                return _.isNil(result) ? null : this.toModel(result)
            }),
        )
    }

    public count(filter?: Object): Observable<number> {
        const promise = this._collection.find(filter).count()
        return from(promise)
    }

    public insert(model: M): Observable<string> {
        const schema = this.toDocument(model)
        const promise = this._collection.insertOne(schema)
        return from(promise).pipe(
            map((result) => {
                return result.insertedId
            }),
            catchError((err) => {
                this._logger.error(`mongo insert error : ${err}`)
                return of(null)
            }),
        )
    }

    public update(id: string, model: M): Observable<boolean> {
        const schema = this.toDocument(model)
        const filter = {
            _id: id,
        }
        const promise = this._collection.updateOne(filter, {
            $set: schema,
        })
        return from(promise).pipe(
            map((result) => {
                return _.isEqual(result.upsertedCount, 1)
            }),
            catchError(() => of(false)),
        )
    }

    public updateByObjectId(id: string, model: M): Observable<boolean> {
        const schema = this.toDocument(model)
        const filter = {
            _id: new ObjectId(id),
        }
        const promise = this._collection.updateOne(filter, {
            $set: schema,
        })
        return from(promise).pipe(
            map((result) => {
                return _.isEqual(result.upsertedCount, 1)
            }),
            catchError(() => of(false)),
        )
    }

    public findAndModify(id: string): Observable<number> {
        const promise = new Promise((resolve, reject) => {
            this._collection.findOneAndUpdate({_id: id}, {$inc: {value: 1}}, {
                returnDocument: 'after',
                upsert: true,
            }, (error, result) => {
                if (error) {
                    reject('Cant upsert findAdnModify')
                }
                resolve(result)
            })
        })
        return from(promise).pipe(
            map(result => {
                const value = _.get(result, 'value.value', 0)
                return _.toNumber(value)
            }),
        )
    }
}
