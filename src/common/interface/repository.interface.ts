import { Observable } from 'rxjs'
import { IEntity } from './entity.interface'
import { IPaginationFilter } from './schema.interface'

export interface IRepository<T> {
    list(page?: number, limit?: number): Observable<T>

    toObservable(input: any): Observable<T>

    find(filter?: Object, pagination?: IPaginationFilter, options?: any, sortBy?: string, sortDirection?: SortDirectionEnum): Observable<T>

    findOne(filter: Object): Observable<T>

    count(filter?: Object): Observable<number>

    insert(model: T): Observable<string>

    update(id: string, model: T): Observable<boolean>

    updateByObjectId(id: string, schema: T): Observable<boolean>

    findAndModify(id: string): Observable<number>
}

export interface IRepositoryMapping<M extends IEntity, S> {
    serialize(model: M): S

    deserialize(schema: S): M
}

export enum SortDirectionEnum {
    ASC = 1,
    DESC = -1,
}

export enum MongoCollectionNameEnum {
    USER_MANAGEMENT = 'user-management',
    PRODUCT = 'product',
    PRODUCT_DETAIL = 'product-detail',
    ORDER = 'order',
}
