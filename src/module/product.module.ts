import { Module } from '@nestjs/common'
import { productRepositoryProviders, productServiceProviders } from '../provider/product.provider'
import { ProductController } from '../controller/product/product.controller'

@Module({
    controllers: [
        ProductController,
    ],
    providers: [
        productServiceProviders,
        ...productRepositoryProviders,
    ],
    imports: [],
    exports: [
        productServiceProviders,
        ...productRepositoryProviders,
    ],
})
export class ProductModule {
}