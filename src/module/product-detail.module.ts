import { Module } from '@nestjs/common'
import { productDetailRepositoryProviders, productDetailServiceProviders } from '../provider/product-detail.provider'
import { ProductDetailController } from '../controller/product-detail/product-detail.controller'

@Module({
    controllers: [
        ProductDetailController,
    ],
    providers: [
        productDetailServiceProviders,
        ...productDetailRepositoryProviders,
    ],
    imports: [],
    exports: [
        productDetailServiceProviders,
        ...productDetailRepositoryProviders,
    ],
})
export class ProductDetailModule {
}