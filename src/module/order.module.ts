import { Module } from '@nestjs/common'
import { productRepositoryProviders } from '../provider/product.provider'
import { orderRepositoryProviders, orderServiceProviders } from '../provider/order.provider'
import { OrderController } from '../controller/order/order.controller'
import { userManagementRepositoryProviders } from '../provider/user-management.provider'

@Module({
    controllers: [
        OrderController,
    ],
    providers: [
        orderServiceProviders,
        ...orderRepositoryProviders,
        ...productRepositoryProviders,
        ...userManagementRepositoryProviders,
    ],
    imports: [],
    exports: [
        orderServiceProviders,
        ...orderRepositoryProviders,
        ...productRepositoryProviders,
        ...userManagementRepositoryProviders,
    ],
})
export class OrderModule {
}