import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { CommonModule } from './common.module'
import { UserManagementModule } from './user-management.module'
import { LoginModule } from './login.module'
import { ProductModule } from './product.module'
import { ProductDetailModule } from './product-detail.module'
import { OrderModule } from './order.module'

@Module({
    imports: [
        ConfigModule.forRoot(),
        CommonModule,
        UserManagementModule,
        LoginModule,
        ProductModule,
        ProductDetailModule,
        OrderModule,
    ]
})

export class MainModule {}
