import { Module } from '@nestjs/common'
import { loginServiceProvider } from '../provider/login.provider'
import { LoginController } from '../controller/login/login.controller'
import { UserManagementModule } from './user-management.module'

@Module({
    controllers: [
        LoginController,
    ],
    imports: [
        UserManagementModule,
    ],
    providers: [
        loginServiceProvider,
    ],
    exports: [
        loginServiceProvider,
    ],
})
export class LoginModule {
}