import { Provider } from '@nestjs/common'
import { providerNames } from './provider.name'
import { Db } from 'mongodb'
import { IRepositoryMapping } from '../common/interface/repository.interface'
import { IConfig } from '../common/interface/config.interface'
import { IProductRepository } from '../domain/product/interface/repository.interface'
import { OrderRepository } from '../repository/order/order.repository'
import { IOrderSchema } from '../repository/order/order.schema'
import { IOrderRepository } from '../domain/order/interface/repository.interface'
import { IOrderModel } from '../domain/order/interface/model.interface'
import { OrderService } from '../domain/order/order.service'
import { IUserManagementRepository } from '../domain/user-management/interface/repository.interface'
import { OrderRepositoryMapping } from '../repository/order/order.mapping'

export const orderRepositoryProviders: Provider[] = [
    {
        provide: providerNames.ORDER_REPOSITORY_MAPPING,
        useClass: OrderRepositoryMapping,
    },
    {
        provide: providerNames.ORDER_REPOSITORY,
        inject: [
            providerNames.MONGO_CONNECTION,
            providerNames.ORDER_REPOSITORY_MAPPING,
        ],
        useFactory: (
            db: Db,
            mapping: IRepositoryMapping<IOrderModel, IOrderSchema>,
        ) => {
            return new OrderRepository(db, mapping)
        },
    },
]

export const orderServiceProviders: Provider = {
    provide: providerNames.ORDER_SERVICE,
    inject: [
        providerNames.CONFIG,
        providerNames.ORDER_REPOSITORY,
        providerNames.PRODUCT_REPOSITORY,
        providerNames.USER_MANAGEMENT_REPOSITORY,
    ],
    useFactory: (
        config: IConfig,
        orderRepository: IOrderRepository,
        productRepository: IProductRepository,
        userManagementRepository: IUserManagementRepository,
    ) => {
        return new OrderService(
            orderRepository,
            productRepository,
            userManagementRepository,
        )
    },
}