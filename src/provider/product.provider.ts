import { Provider } from '@nestjs/common'
import { providerNames } from './provider.name'
import { Db } from 'mongodb'
import { IRepositoryMapping } from '../common/interface/repository.interface'
import { IConfig } from '../common/interface/config.interface'
import { ProductRepository } from '../repository/product/product.repository'
import { IProductModel } from '../domain/product/interface/model.interface'
import { ProductService } from '../domain/product/product.service'
import { IProductRepository } from '../domain/product/interface/repository.interface'
import { IProductSchema } from '../repository/product/product.schema'
import { ProductRepositoryMapping } from '../repository/product/product.mapping'

export const productRepositoryProviders: Provider[] = [
    {
        provide: providerNames.PRODUCT_REPOSITORY_MAPPING,
        useClass: ProductRepositoryMapping,
    },
    {
        provide: providerNames.PRODUCT_REPOSITORY,
        inject: [
            providerNames.MONGO_CONNECTION,
            providerNames.PRODUCT_REPOSITORY_MAPPING,
        ],
        useFactory: (
            db: Db,
            mapping: IRepositoryMapping<IProductModel, IProductSchema>,
        ) => {
            return new ProductRepository(db, mapping)
        },
    },
]

export const productServiceProviders: Provider = {
    provide: providerNames.PRODUCT_SERVICE,
    inject: [
        providerNames.CONFIG,
        providerNames.PRODUCT_REPOSITORY,
    ],
    useFactory: (
        config: IConfig,
        productRepository: IProductRepository,
    ) => {
        return new ProductService(
            productRepository,
        )
    },
}