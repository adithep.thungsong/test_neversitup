enum CommonProviderNames {
    CONFIG = 'common-config-provider',
    MONGO_CONNECTION = 'mongo-connection-provider',
}

enum JWTServiceProviderNames {
    JWT_SERVICE = 'jwt-service-provider',
    JWT_STRATEGY = 'jwt-strategy-service-provider'
}

enum LoginServiceProviderNames {
    LOGIN_SERVICE = 'login-service-provider'
}

enum UserManagementProviderNames {
    USER_MANAGEMENT_REPOSITORY = 'user-management-repository-provider',
    USER_MANAGEMENT_REPOSITORY_MAPPING = 'user-management-repository-mapping-provider',
    USER_MANAGEMENT_SERVICE = 'user-management-service-provider'
}

enum ProductProviderNames {
    PRODUCT_REPOSITORY = 'product-repository-provider',
    PRODUCT_REPOSITORY_MAPPING = 'product-repository-mapping-provider',
    PRODUCT_SERVICE = 'product-service-provider'
}

enum ProductDetailProviderNames {
    PRODUCT_DETAIL_REPOSITORY = 'product-detail-repository-provider',
    PRODUCT_DETAIL_REPOSITORY_MAPPING = 'product-detail-repository-mapping-provider',
    PRODUCT_DETAIL_SERVICE = 'product-detail-service-provider'
}

enum OrderProviderNames {
    ORDER_REPOSITORY = 'order-repository-provider',
    ORDER_REPOSITORY_MAPPING = 'order-repository-mapping-provider',
    ORDER_SERVICE = 'order-service-provider'
}

export const providerNames = Object.assign({},
    CommonProviderNames,
    JWTServiceProviderNames,
    UserManagementProviderNames,
    LoginServiceProviderNames,
    ProductProviderNames,
    ProductDetailProviderNames,
    OrderProviderNames,
)
