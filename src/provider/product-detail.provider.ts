import { Provider } from '@nestjs/common'
import { providerNames } from './provider.name'
import { Db } from 'mongodb'
import { IRepositoryMapping } from '../common/interface/repository.interface'
import { IConfig } from '../common/interface/config.interface'
import { IProductDetailRepository } from '../domain/product-detail/interface/repository.interface'
import { ProductDetailRepositoryMapping } from '../repository/product-detail/product-detail.mapping'
import { ProductDetailService } from '../domain/product-detail/product.service'
import { IProductDetailSchema } from '../repository/product-detail/product-detail.schema'
import { ProductDetailRepository } from '../repository/product-detail/product-detail.repository'
import { IProductDetailModel } from '../domain/product-detail/interface/model.interface'

export const productDetailRepositoryProviders: Provider[] = [
    {
        provide: providerNames.PRODUCT_DETAIL_REPOSITORY_MAPPING,
        useClass: ProductDetailRepositoryMapping,
    },
    {
        provide: providerNames.PRODUCT_DETAIL_REPOSITORY,
        inject: [
            providerNames.MONGO_CONNECTION,
            providerNames.PRODUCT_DETAIL_REPOSITORY_MAPPING,
        ],
        useFactory: (
            db: Db,
            mapping: IRepositoryMapping<IProductDetailModel, IProductDetailSchema>,
        ) => {
            return new ProductDetailRepository(db, mapping)
        },
    },
]

export const productDetailServiceProviders: Provider = {
    provide: providerNames.PRODUCT_DETAIL_SERVICE,
    inject: [
        providerNames.CONFIG,
        providerNames.PRODUCT_DETAIL_REPOSITORY,
    ],
    useFactory: (
        config: IConfig,
        productDetailRepository: IProductDetailRepository,
    ) => {
        return new ProductDetailService(
            productDetailRepository,
        )
    },
}